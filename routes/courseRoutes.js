const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require('../auth.js');


//Route for creating a course
router.post("/", auth.verify, (req, res) => {

		const isAdmin= auth.decode(req.headers.authorization).isAdmin;
		console.log(isAdmin)

		if(isAdmin){
			courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));

		}
		else{
			return res.send("not authorized to add courses")
		}

	

});

//route for retrieving all the courses
router.get("/all", (req,res)=>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// create a route for retrieving active courses
router.get("/", (req,res)=>{

	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// route for retrieving specific courses
router.get("/:courseId", (req,res) =>{///:courseId" sets it for request params
	//console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController=> res.send(resultFromController));
});

//route for updating a course
router.put("/:courseId", auth.verify, (req,res) => {

	const userData= auth.decode(req.headers.authorization)
	
	courseController.updateCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
})


//route for Archiving a course
router.put("/:courseId/archive", auth.verify, (req,res) => {

	const archiveData= auth.decode(req.headers.authorization)
					 
	courseController.archiveCourse(req.params, req.body, archiveData).then(resultFromController => res.send(resultFromController))
})


module.exports=router;

/*
mini activity:
limit the course creation to admin only.
refactor the course route/ controller

*/